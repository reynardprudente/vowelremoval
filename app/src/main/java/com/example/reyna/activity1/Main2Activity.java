package com.example.reyna.activity1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.reyna.activity1.R;


/**
 * Created by reyna on 7/22/2017.
 */
public class Main2Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_2);
        int i, ctr;
        TextView c = (TextView) findViewById(R.id.textView2);
        TextView a = (TextView) findViewById(R.id.textView4);
        Bundle bundle = getIntent().getExtras();
        String str1 = bundle.getString("data");
        final String str2 = str1.replaceAll("[aeiouAEIOU]", "");
        int cb = 0, cc = 0, cd = 0, cf = 0, cg = 0, cho = 0, cj = 0, ck = 0, cl = 0, cm = 0, cn = 0, cp = 0, cq = 0, cr = 0, cs = 0, ct = 0, cv = 0, cw = 0, cx = 0, cy = 0, cz = 0;
        char ch;
        c.setText(str2);

        for (i = 0; i < str2.length(); i++) {
           ch=str2.charAt(i);
            switch(ch)
            {
                case 'b':case'B': cb++;break;
                case 'c':case'C': cc++;break;
                case 'd':case'D': cd++;break;
                case 'f':case'F': cf++;break;
                case 'g':case'G': cg++;break;
                case 'h':case'H': cho++;break;
                case 'j':case'J': cj++;break;
                case 'k':case'K': ck++;break;
                case 'l':case'L': cl++;break;
                case 'm':case'M': cm++;break;
                case 'n':case'N': cn++;break;
                case 'p':case'P': cp++;break;
                case 'q':case'Q': cq++;break;
                case 'r':case'R': cr++;break;
                case 's':case'S': cs++;break;
                case 't':case'T': ct++;break;
                case 'v':case'V': cv++;break;
                case 'w':case'W': cw++;break;
                case 'x':case'X': cx++;break;
                case 'y':case'Y': cy++;break;
                case 'z':case'Z': cz++;break;

            }
        }
        a.setText("b  ="+cb+"\nc=  "+cc+"\nd  ="+cd+"\nf  ="+cf+"\ng  ="+cg+"\nh  ="+cho+"\nj  ="+cj+"\nk  ="+ck+"\nl  ="+cl+"\nm  ="+cm+
        "\nn  ="+cn+"\np  ="+cp+"\nq  ="+cq+"\nr  ="+cr+"\ns  ="+cs+"\nt  ="+ct+"\nv  ="+cv+"\nw  ="+cw+"\nx  ="+cx+"\ny  ="+cy+"\nz  ="+cz);
    }
}








